package me.mrreasole.chatmanager.channels;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;



public class SupportChannel extends Channel
{
	public SupportChannel()
	{
		this.name = "Support";
		this.color = ChatColor.BLUE;
	}
	
	@Override
	public boolean join(Player p)
	{
		for(Player tmp : players)
		{
			tmp.sendMessage(ChatColor.GREEN + p.getName() + " hat den Channel betreten!");
		}
		
		players.add(p);
		
		p.sendMessage(ChatColor.GREEN + "Du hast den Channel betreten!");
		
		return true;
	}
}