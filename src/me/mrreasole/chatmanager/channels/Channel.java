package me.mrreasole.chatmanager.channels;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

public class Channel
{
	protected List<Player> players = new ArrayList<Player>();
	protected List<Player> invited = new ArrayList<Player>();
	protected String name;
	protected ChatColor color;
	
	protected Channel()
	{
		
	}
	
	public Channel(String Name, Player p)
	{
		players.add(p);
		this.name = Name;
	}
	
	public String getName()
	{
		return name;
	}
	
	public boolean join(Player p)
	{
		if((invited.contains(p) || p.isOp() || p.hasPermission("chatmanager.joinall")) && !players.contains(p))
		{
			for(Player tmp : players)
			{
				tmp.sendMessage(ChatColor.GREEN + p.getName() + " hat den Channel betreten!");
			}
			
			players.add(p);
			
			if(invited.contains(p))
			{
				invited.remove(p);
			}
			
			p.sendMessage(ChatColor.GREEN + "Du hast den Channel betreten!");
			
			return true;
		}
		else
		{
			return false;
		}
	}
	
	public boolean leave(Player p)
	{
		if(players.contains(p))
		{
			players.remove(p);
			
			for(Player tmp : players)
			{
				tmp.sendMessage(ChatColor.GREEN + p.getName() + " hat den Channel verlassen.");
			}
			
			p.sendMessage(ChatColor.GREEN + "Du hast den Channel verlassen.");
			return true;
		}
		
		
		return false;
	}
	
	public boolean contains(Player p)
	{
		if(players.contains(p)) return true;
		else return false;
	}
	
	public boolean invite(Player p)
	{
		if(!invited.contains(p) && !players.contains(p))
		{
			invited.add(p);
			p.sendMessage(ChatColor.GREEN + "Du wurdest in den Channel \"" +  name + "\" eingeladen.");
			return true;
		}
		else
		{
			return false;
		}
	}
	
	public boolean hasInvited(Player p)
	{
		if(invited.contains(p))
		{
			return true;
		}
		return false;
	}
	
	public boolean unInvite(Player p)
	{
		if(invited.contains(p))
		{
			p.sendMessage(ChatColor.RED + "Die Einladung zu dem Channel " + name + " wurde zur�ckgezogen.");
			invited.remove(p);
		}
		
		return false;
	}
	
	public boolean setColor(String s)
	{
		ChatColor tmp = color;
		
		color = ChatColor.getByChar(s);
		
		if(color == null)
		{
			color = tmp;
			return false;
		}
		return true;
	}
	
	public void sendMessage(String s, Player p)
	{
		for(Player tmp : players)
		{
			tmp.sendMessage(ChatColor.RED + "[" + p.getName() + "@" + name + "]" + color + s);
		}
	}
	
	
}
