package me.mrreasole.chatmanager;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import me.mrreasole.chatmanager.channels.Channel;

import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.FireworkEffect;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Firework;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.meta.FireworkMeta;
import org.kitteh.tag.PlayerReceiveNameTagEvent;


public class ChatEvent implements Listener
{
	ChatManager plugin;
	FileConfiguration cfg;
	
	public List<String> allowedIps = new ArrayList<>();
	public List<String> wordBlacklist = new ArrayList<>();
	
	public ChatEvent(ChatManager arg1, FileConfiguration arg2)
	{	
		this.plugin = arg1;
		this.cfg = arg2;
		
		try { readWhitelist(); readBlacklist(); }
		catch(Exception e) { e.printStackTrace(); }
		
		plugin.getServer().getPluginManager().registerEvents(this, plugin);
	}
	
	private void readBlacklist() throws Exception
	{
		File f = new File(plugin.getDataFolder() + "/wordBlacklist.txt");
		
		if(!f.exists())
		{
			f.createNewFile();
			
			PrintWriter pw = new PrintWriter(f);
			
			for(String s : blacklistedStandart)
			{
				pw.println(s);
			}
			
			pw.flush();
			pw.close();
		}
		
		BufferedReader br = new BufferedReader(new FileReader(f));
		
		for(String in = br.readLine(); in != null; in = br.readLine())
		{
			if(in.length() > 0)
			{
				wordBlacklist.add(in);
			}
		}
		
		br.close();
	}
	
	private void readWhitelist() throws Exception
	{
		File f = new File(plugin.getDataFolder() + "/ipWhitelist.txt");
		
		if(!f.exists())
		{
			f.createNewFile();
			
			PrintWriter pw = new PrintWriter(f);
			
			for(String s : allowedStandart)
			{
				pw.println(s);
			}
			
			pw.flush();
			pw.close();
		}
		
		BufferedReader br = new BufferedReader(new FileReader(f));
		
		for(String in = br.readLine(); in != null; in = br.readLine())
		{
			if(in.length() > 0)
			{
				allowedIps.add(in);
			}
		}
		
		br.close();
	}
	
	@EventHandler
	public void onChat(AsyncPlayerChatEvent e)
	{
		Player p = e.getPlayer();
		String message = e.getMessage();
		if(p.hasPermission("tc.chat"))
		{
			message = ChatColor.translateAlternateColorCodes('&', message);
		}
		e.setMessage(message);
		
		int counter = 0;
	    
		boolean bypass = false;
		
		if(p.hasPermission("SpamManager.bypass") || p.isOp())
		{
			bypass = true;
		}
		
		if(!e.isCancelled() && !bypass)
		{
			Pattern pattern = Pattern.compile(".*" + "[lL]+" + "[aA]+" + "[gG]+" + ".*");
			Matcher matcher = pattern.matcher(message);
			if(matcher.find())
			{
				p.sendMessage(ChatColor.RED + "Es ist verboten worden \"lag\" zu schreiben, da viele Spieler es damit etwas �bertreiben.");
				e.setCancelled(true);
			}
		}
		
		
		
		if (!e.isCancelled() && !bypass)
		{
			if (plugin.PlayerList.contains(p))
			{
				p.sendMessage(ChatColor.RED + "" + ChatColor.BOLD + "Bitte schreib nicht so schnell.");
				
				e.setCancelled(true);
			}
		}
		
		
		if (!e.isCancelled() && !bypass)
		{
			List<String> BadWordsList = cfg.getStringList("Not Allowed Words");
			String[] BadWordsArray = (String[])BadWordsList.toArray(new String[BadWordsList.size()]);
			
			counter = 0;
			while (counter < BadWordsArray.length)
			{
				String message2 = message.toLowerCase();
				String BadWord = BadWordsArray[counter].toLowerCase();
				
				boolean b = message2.contains(" " + BadWord + " ");
				
				if(b)
				{
					p.sendMessage(ChatColor.RED + "" + ChatColor.BOLD + "Das Schimpfwort " + BadWord + " wurde erkannt. Bitte nutze dies nicht.");
					
					this.plugin.getServer().dispatchCommand(this.plugin.getServer().getConsoleSender(), "kick " + p.getName() + 
							" Schimpfwort/e");
					
					e.setCancelled(true);
				}
				counter++;
			}
		}
		
		if (!e.isCancelled() && !bypass)
		{
			char[] tmp = "ABCDEFGHIJKLMNOPQRSTUVWXYZ���".toCharArray();
			String[] Caps = new String[30];
			int i = 0;
			while (i < tmp.length)
			{
				Caps[i] = ("" + tmp[i]);
				i++;
			}
			
			counter = 0;
			int CapsLetters = 0;
			while (counter < Caps.length)
			{
				CapsLetters += getFrequency(message, Caps[counter]);
				counter++;
			}
			
			if (CapsLetters > 6 || CapsLetters == message.length())
			{
				p.sendMessage(ChatColor.RED + "" + ChatColor.BOLD + "Bitte schreibe nicht in Capslock.");
				
				message = message.toLowerCase();
				
				e.setMessage(message);
			}
		}
		
		if (!e.isCancelled() && !bypass)
		{
			boolean AllowedIP = false;
			
			for(String allowed : allowedIps)
			{
				if (message.toLowerCase().contains(allowed.toLowerCase()))
				{
					AllowedIP = true;
				}
			}
			
			Pattern pattern = Pattern.compile("(?i)(((([a-zA-Z0-9-]+\\.)+(de|eu|com|net|to|gs|me|info|biz|tv|au))|([0-9]{1,3}\\.){3}[0-9]{1,3})(\\:[0-9]{2,5})?)");
			Matcher matcher = pattern.matcher(message);
			
			if ((matcher.find()) && (!AllowedIP))
			{
				p.sendMessage(ChatColor.RED + "Es wurde erkannt dass du Werbung f�r den Server " + ChatColor.BOLD + matcher.group() + ChatColor.RESET + ChatColor.RED + 
						" machen m�chtest. Sollte dies ein falscher " +
						"Alarm sein, mache einen Screenshot von dieser Nachricht und zeige diesen einem Teammitglied.");
				
				this.plugin.getServer().dispatchCommand(this.plugin.getServer().getConsoleSender(), "warn " + p.getName() + 
						" Serverwerbung");
				
				e.setCancelled(true);
			}
		}
		
		
		for(Channel tmp : plugin.channels)
		{
			if(tmp.contains(p))
			{
				tmp.sendMessage(e.getMessage(), p);
				e.setCancelled(true);
				return;
			}
		}
		
		e.setFormat(formatMessage(message, p.getName(), plugin.servername));
		
		plugin.msgSender.forwardChatMessage(e.getMessage(), p.getName());
		
		if (!e.isCancelled() && !bypass)
		{
			//Wait(p);
		}
	}
	
	
	public String formatMessage(String message, String pName, String server)
	{
		String rank = plugin.sql.getRankFromPlayer(pName);
		if (rank.length() > 0)
		{
			String prefix = plugin.sql.getPrefix(rank);
			String suffix = plugin.sql.getSuffix(rank);
			prefix = ChatColor.translateAlternateColorCodes('&', prefix);
			suffix = ChatColor.translateAlternateColorCodes('&', suffix);
			message = prefix + pName + suffix + " " + message;
		}
		else
		{
			message = "�8[�6Spieler�8|�7" + pName + "�8]�6: �7" + message;
		}
		
		message = ChatColor.DARK_GRAY + "[" + ChatColor.GOLD + server + ChatColor.DARK_GRAY  + "]" + 
				ChatColor.RESET + message;
		
		return message;
	}
	
	
	@EventHandler
	public void onNameTag(PlayerReceiveNameTagEvent e)
	{
		e.setTag(e.getNamedPlayer().getPlayerListName());
	}
	
	
	
	@EventHandler
	public void onQuit(PlayerQuitEvent e)
	{
		Player player = e.getPlayer();
		
		for(Channel tmp : plugin.channels)
		{
			if(tmp.contains(player))
			{
				tmp.leave(player);
				
				break;
			}
			else if(tmp.hasInvited(player))
			{
				tmp.unInvite(player);
				
				break;
			}
		}
		
		String rank = plugin.sql.getRankFromPlayer(player.getName());
		if (rank != null)
		{
			plugin.setColor(player);
			e.setQuitMessage(player.getDisplayName() + " �8hat das Spiel verlassen.");
		}
		else
		{
			e.setQuitMessage("");
		}
	}
	
	
	@EventHandler
	public void onJoin(PlayerJoinEvent e)
	{
		Player p = e.getPlayer();
		
		if(!plugin.isWhitelisted(p))
		{
			StringBuilder sb = new StringBuilder();
			
			for(String str : plugin.whl.getWhiteList())
			{
				sb.append(str + ", ");
			}
			
			p.kickPlayer("Du ben�tigst einen der folgenden R�nge,\num auf dem Server spielen zu d�rfen:\n" + sb.toString());
		}
		
		p.sendMessage("�3�l----- �9Wilkommen auf Hexxagon, �l" + p.getName() + " �3�l-----");
		p.sendMessage("�8�l> �aWenn du Fragen hast, ");
		p.sendMessage("  �a�l/faq �aoder wende dich an einen �lMod/Admin.");
		p.sendMessage("\n�8�l> �aDer Server ist Freebuild,\n   erstelle dein eigenes GS mit �l/gs�a.\n");
		p.sendMessage("\n�8�l> �aEs gibt mehrere Dimensionen,\n   die Warps findest du am Spawn mit �l/spawn�a.");
		p.sendMessage("\n�3Vote f�r uns auf�e vote.hexxit.de �3um eine Belohnung zu erhalten.");
		p.sendMessage("�3Unser Forum findest du unter�e www.hexxit.de");
		p.sendMessage("�3Unseren TS-Server fidest du unter�e ts.hexxit.de");
		p.sendMessage("\n�6Viel Spa� auf deinen Abenteuern,");
		p.sendMessage("�6w�nscht dir das Hexxagon Team!");
		
		try
		{
			
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		if (p.hasPlayedBefore())
		{
			String rank = plugin.sql.getRankFromPlayer(p.getName());
			if (rank != null)
			{
				plugin.setColor(p);
				e.setJoinMessage(p.getDisplayName() + " �8ist dem Spiel beigetreten.");
			}
			else
			{
				/*
				plugin.setColor(p);
				for (Player ap : plugin.getServer().getOnlinePlayers())
				{
					ap.sendMessage("�6" + p.getName() + " �8ist dem Spiel beigetreten.");
				}
				*/
				e.setJoinMessage("");
			}
		}
		else
		{
			plugin.setColor(p);
			e.setJoinMessage("�3Willkommen �b�l" + e.getPlayer().getName() + " �3auf dem ersten deutschen Hexxitserver.");
			e.getPlayer().sendMessage("");
			
			Firework firework = (Firework)e.getPlayer().getLocation().getWorld().spawn(e.getPlayer().getLocation(), Firework.class);
			FireworkMeta effect = firework.getFireworkMeta();
			effect.addEffects(new FireworkEffect[] { FireworkEffect.builder().with(FireworkEffect.Type.BALL_LARGE).withColor(Color.BLACK).with(FireworkEffect.Type.BALL_LARGE).withColor(Color.WHITE).withTrail().with(FireworkEffect.Type.BALL).withColor(Color.GRAY).build() });
			effect.setPower(0);
			firework.setFireworkMeta(effect);
		}
	}
	
	
	private static int getFrequency(String source, String part)
	{
		if ((source == null) || (source.isEmpty()) || (part == null) || (part.isEmpty()))
		{
			return 0;
		}
		
		int count = 0;
		int pos = 0;
		
		while ((pos = source.indexOf(part, pos)) != -1)
		{
			pos += part.length();
			count++;
		}
		
		return count;
	}
	
	/*
	private void Wait(final Player p)
	{
		new Thread()
		{
			public void run()
			{
				try
				{
					plugin.PlayerList.add(p);
					
					Thread.sleep(2000);
					
					plugin.PlayerList.remove(p);
				}
				catch (InterruptedException localInterruptedException)
				{
				}
			}
		}.start();
	}
	*/
	
	private String[] blacklistedStandart = new String[]
			{
			"Arschloch",
			"Wixxer",
			"Wichser",
			"Hurensohn",
			"Hure",
			"Hurenkind",
			"Nigga",
			"Nigger",
			"Kinderficker",
			"Ficker",
			"Muschi",
			"Penis",
			"Mutterficker",
			"Hitler",
			"Nazi",
			"Sieg Heil",
			"Idiot",
			"Penner",
			"Arschgeige",
			"Arschgesicht",
			"Arschmarde",
			"Drecksau",
			"Hackfresse",
			"Nutte",
			"Pissnelke",
			"Schlampe",
			"Kacklappen",
			"Trockenfurzer",
			"Fettarsch",
			"Bl�dmann",
			"Kotzbrocken"
			};
	
	private String[] allowedStandart = new String[]
			{
			"www.hexxit.de",
			"www.technicserver.de",
			"vote.hexxit.de",
			"hexxit.de",
			"minecraft.de",
			"mojang.net"
			};
}