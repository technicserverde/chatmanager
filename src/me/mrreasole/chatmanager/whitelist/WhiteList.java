package me.mrreasole.chatmanager.whitelist;

import java.util.List;

public class WhiteList
{
	private List<String> whlranks = null;
	private boolean enabled;
	
	public WhiteList(List<String> ranks, boolean enabled)
	{
		this.whlranks = ranks;
		this.setEnabled(enabled);
	}
	
	public boolean isWhitelisted(String rank)
	{
		if(this.isEnabled())
		{
			return whlranks.contains(rank);
		}
		else
		{
			return true;
		}
	}
	
	public List<String> getWhiteList()
	{
		return whlranks;
	}
	
	public boolean isEnabled()
	{
		return enabled;
	}
	
	public void setEnabled(boolean enabled)
	{
		this.enabled = enabled;
	}
}