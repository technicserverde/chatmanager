package me.mrreasole.chatmanager;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import me.mrreasole.chatmanager.bungee.Messager;
import me.mrreasole.chatmanager.channels.Channel;
import me.mrreasole.chatmanager.channels.SupportChannel;
import me.mrreasole.chatmanager.sql.SQLConnection;
import me.mrreasole.chatmanager.whitelist.WhiteList;

import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;


public class ChatManager extends JavaPlugin implements CommandExecutor
{
	ChatManager plugin;
	FileConfiguration cfg;
	public List<Channel> channels = new ArrayList<Channel>();
	//Scoreboard score;
	public List<Player> PlayerList = new ArrayList<Player>();
	public SQLConnection sql = null;
	public ChatEvent evt;
	public Messager msgSender;
	public WhiteList whl;
	
	String[] specialChar = { "\\&", "\\&", "\\[", "\\[", "\\]", "\\]", ":", "\\:" };
	public static final String bungeeChannel = "BungeeCord";
	public static final String chatChannel = "BungeeChat";
	public static final String splitChar = "�";
	public String servername;
	
	@Override
	public void onEnable()
	{
		this.plugin = this;
		
		loadConfig();
		
		this.servername = getConfig().getString("ChatManager.Servername");
		
		loadSQLConnection();
		
		this.evt = new ChatEvent(this, cfg);
		msgSender = new Messager(this);
		
		try
		{
			initWhiteList();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		/*
		this.score = Bukkit.getScoreboardManager().getMainScoreboard();
		if (this.score.getTeam("Spieler") == null)
		{
			this.score.registerNewTeam("Spieler");
		}
		*/
		channels.add(new SupportChannel());
	}
	
	@Override
	public void onDisable()
	{
		sql.close();
	}
	/*
	public Scoreboard getScoreboard()
	{
		return score;
	}
	*/
	private void initWhiteList() throws IOException
	{
		File f = new File(plugin.getDataFolder() + "/RankWhitelist.txt");
		
		if(!f.exists())
		{
			f.createNewFile();
		}
		
		List<String> ranks = new ArrayList<>();
		
		BufferedReader br = new BufferedReader(new FileReader(f));
		for(String in = br.readLine(); in != null; in = br.readLine())
		{
			if(in.length() > 0)
			{
				ranks.add(in);
			}
		}
		
		br.close();
		
		this.whl = new WhiteList(ranks, getConfig().getBoolean("ChatManager.Use Whitelist"));
	}
	
	private void loadConfig()
	{
		cfg = this.getConfig();
		
		getConfig().addDefault("ChatManager.Mysql.host", "localhost");
		getConfig().addDefault("ChatManager.Mysql.port", "3306");
		getConfig().addDefault("ChatManager.Mysql.user", "bukkit");
		getConfig().addDefault("ChatManager.Mysql.pass", "walrus");
		getConfig().addDefault("ChatManager.Mysql.db", "minecraft");
		
		getConfig().addDefault("ChatManager.Servername", "ABC");
		
		getConfig().addDefault("ChatManager.Use Whitelist", false);
		
		getConfig().options().copyDefaults(true);
		saveConfig();
	}
	
	public void loadSQLConnection()
	{
	    String host = getConfig().getString("ChatManager.Mysql.host");
	    String port = getConfig().getString("ChatManager.Mysql.port");
	    String user = getConfig().getString("ChatManager.Mysql.user");
	    String pass = getConfig().getString("ChatManager.Mysql.pass");
	    String db = getConfig().getString("ChatManager.Mysql.db");
	    try {
			this.sql = new SQLConnection(host, port, user, pass, db);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    this.sql.setChatManager(this);
	}
	
	public boolean isWhitelisted(Player p)
	{
		return whl.isWhitelisted(sql.getRankFromPlayer(p.getName()));
	}
	
	@Override
	public boolean onCommand(CommandSender s, Command cmd, String label, String[] args)
	{
		if(cmd.getName().equalsIgnoreCase("channel") || cmd.getName().equalsIgnoreCase("ch"))
		{
			if(!(s instanceof Player))
			{
				s.sendMessage(ChatColor.RED + "Dieser Command ist nur als Spieler ausf�hrbar!");
				return true;
			}
			
			Player p = (Player) s;
			
			if(args.length == 0)
			{
				s.sendMessage(ChatColor.BOLD + "" + ChatColor.BLUE + "M�gliche Befehle:");
				s.sendMessage(ChatColor.GREEN + " - /ch list");
				s.sendMessage(ChatColor.GREEN + " - /ch create <Channel>");
				s.sendMessage(ChatColor.GREEN + " - /ch join <Channel>");
				s.sendMessage(ChatColor.GREEN + " - /ch leave");
				s.sendMessage(ChatColor.GREEN + " - /ch invite <Spieler>");
				s.sendMessage(ChatColor.GREEN + " - /a <Nachricht>");
				return true;
			}
			else if(args.length == 1)
			{
				if(args[0].equalsIgnoreCase("list"))
				{
					p.sendMessage(ChatColor.GRAY + "[#--------------------Channels--------------------#]");
					for(Channel c : channels)
					{
						p.sendMessage(ChatColor.BLUE + "  - " + c.getName());
					}
					p.sendMessage(ChatColor.GRAY + "[#--------------------Channels--------------------#]");
				}
				else if(args[0].equalsIgnoreCase("leave"))
				{
					for(Channel tmp : channels)
					{
						if(tmp.contains(p))
						{
							tmp.leave(p);
							return true;
						}
					}
					
					p.sendMessage(ChatColor.RED + "Du bist zur Zeit in keinem Channel.");
				}
			}
			else if(args.length == 2)
			{
				if(args[0].equalsIgnoreCase("create"))
				{
					for(Channel tmp : channels)
					{
						if(tmp.getName().equalsIgnoreCase(args[1]))
						{
							p.sendMessage(ChatColor.RED + "Diesen Channel gibt es bereits!");
							return true;
						}
					}
					
					Channel c = new Channel(args[1], p);
					p.sendMessage(ChatColor.GREEN + "Channel erfolgreich erstellt!");
					p.sendMessage(ChatColor.GREEN + "Du hast den Channel betreten!");
					channels.add(c);
				}
				else if(args[0].equalsIgnoreCase("invite"))
				{
					for(Channel tmp : channels)
					{
						if(tmp.contains(p))
						{
							if(tmp.invite(getServer().getPlayer(args[1])))
							{
								p.sendMessage(ChatColor.GREEN + "Die Einladung wurde verschickt!");
								return true;
							}
							else
							{
								p.sendMessage(ChatColor.RED + "Dieser Spieler wurde bereits eingeladen.");
								return true;
							}
						}
					}
				}
				else if(args[0].equalsIgnoreCase("join"))
				{
					for(Channel tmp : channels)
					{
						if(tmp.contains(p))
						{
							p.sendMessage(ChatColor.RED + "Du bist bereits in einem Channel!");
							return true;
						}
					}
					
					for(Channel tmp : channels)
					{
						if(tmp.getName().equalsIgnoreCase(args[1]))
						{
							if(tmp.join(p))
							{
								return true;
							}
							else
							{
								p.sendMessage(ChatColor.GREEN + "Du ben�tigst eine Einladung um den Channel zu betreten.");
								return true;
							}
						}
					}
					
					p.performCommand("ch list");
				}
			}
		}
		
		if(cmd.getName().equalsIgnoreCase("a"))
		{
			if(!(s instanceof Player))
			{
				s.sendMessage(ChatColor.RED + "Dieser Command ist nur als Spieler ausf�hrbar!");
				return true;
			}
			
			Player p = (Player) s;
			
			StringBuilder sb = new StringBuilder();
			
			for(int i = 1; i < args.length; i++)
			{
				sb.append(args[i] + " ");
			}
			
			String msg = evt.formatMessage(sb.toString(), p.getName(), this.servername);
			
			for(Player tmp : this.getServer().getOnlinePlayers())
			{
				tmp.sendMessage(msg);
			}
		}
		
		if(cmd.getName().equalsIgnoreCase("support"))
		{
			if(!(s instanceof Player))
			{
				s.sendMessage(ChatColor.RED + "Dieser Command ist nur als Spieler ausf�hrbar!");
				return true;
			}
			
			Player p = (Player) s;
			
			StringBuilder sb = new StringBuilder();
			
			for(int i = 0; i < args.length; i++)
			{
				sb.append(args[i] + " ");
			}
			
			String msg = evt.formatMessage(ChatColor.BLUE + sb.toString(), p.getName(), this.servername);
			
			for(Player tmp : this.getServer().getOnlinePlayers())
			{
				tmp.sendMessage(msg);
			}
		}
		
		if(cmd.getName().equalsIgnoreCase("me"))
		{
			if(!s.hasPermission("basicmanager.me"))
			{
				s.sendMessage(ChatColor.RED + "Du hast keine Berechtigung um das zu tun");
				return true;
			}
			
			if(args.length >= 1)
			{
				if(!(s instanceof Player))
				{
					s.sendMessage(ChatColor.RED + "Du kannst das nur als Spieler ausf�hren.");
					return true;
				}
				
				StringBuilder sb = new StringBuilder();
				
				for(String str : args)
				{
					sb.append(str + " ");
				}
				
				for(Player p : plugin.getServer().getOnlinePlayers())
				{
					p.sendMessage(((Player) s).getDisplayName() + " " + sb.toString());
				}
				
				return true;
			}
		}
		
		if (cmd.getName().equalsIgnoreCase("chat"))
		{
			if ((s.hasPermission("basic.gm")) || (s.isOp()) || (s.hasPermission("ChatManager.chat")))
			{
				if (args.length == 0)
				{
					s.sendMessage("�a�lKommandos:\n�2- /chat setrank <Player <Rank>\n- /chat remrank <Player>");
				}
				else if (args.length >= 1)
				{
					if (args[0].equalsIgnoreCase("addrank"))
					{
						if ((s.getName().equals("CrashCraftler")) || (s.getName().equals("dertreifel")) || ((s instanceof ConsoleCommandSender)))
						{
							if (args.length == 1)
							{
								s.sendMessage("�a/chat addrank <Name> <Color> <Prefix> <Suffix>");
								s.sendMessage("�aName: �8Dies dient nur der Identifikation in SQL.");
								s.sendMessage("�aColor: �8Dies ist die Farbe im Chat/Tablist und �ber dem Kopf.");
								s.sendMessage("�aPrefix: �8Dies beinhaltet alles was �lvor �8dem Spielernamen steht.");
								s.sendMessage("�aSuffix: �8Dies beinhaltet alles was �lhinter �8dem Spielernamen steht.");
								s.sendMessage("\n�aBeispiel:\n�7/chat addrank <admin> <&4> <&8[&cAdmin&8|> <&8]&4: ");
								s.sendMessage("�cAchtung: �7Die Argumente d�rfen keine leerzeichen beinhalten!");
							}
							else
							{
								String name = args[1];
								if (!IsRankNameValid(name))
								{
									s.sendMessage("�cDer Rangname darf nich l�nger als 20 Zeichen sein!");
									return true;
								}
								String color = args[2];
								
								String prefix = args[3];
								String suffix = args[4];
								
								if (!this.sql.rankExists(name))
								{
									this.sql.addRank(escapeSpecialChar(name), escapeSpecialChar(color), escapeSpecialChar(prefix), escapeSpecialChar(suffix));
									s.sendMessage("�7Der Rang " + ChatColor.translateAlternateColorCodes('&', color) + "�l" + name + " �7wurde registriert.");
								}
								else
								{
									s.sendMessage("�cDieser Rang ist bereits registriert.");
								}
							}
						}
						else s.sendMessage("�cDu hast keine berechtigung dieses Kommando auszuf�hren!");
					}
					else if (args[0].equalsIgnoreCase("delrank"))
					{
						if ((s.getName().equals("CrashCraftler")) || (s.getName().equals("dertreifel")) || ((s instanceof ConsoleCommandSender)))
						{
							if (args.length != 1)
							{
								String rank = args[1];
								if (this.sql.rankExists(rank))
								{
									s.sendMessage("�7Der Rang " + ChatColor.translateAlternateColorCodes('&', this.sql.getColor(rank)) + "�l" + rank + " �7wurde gel�scht.");
									this.sql.delRank(rank);
								}
								else
								{
									s.sendMessage("�cDieser Rang ist nicht registriert");
								}
							}
							else
							{
								s.sendMessage("�7/chat delrank <RankName>");
							}
						}
						else
						{
							s.sendMessage("�cDu hast keine berechtigung dieses Kommando auszuf�hren!");
						}
					}
					else if (args[0].equalsIgnoreCase("setrank"))
					{
						if (args.length != 1)
						{
							String rank = args[2];
							if (this.sql.rankExists(rank))
							{
								OfflinePlayer t = getServer().getOfflinePlayer(args[1]);
								String color = this.sql.getColor(rank);
								try
								{
									this.sql.setRankToPlayer(t, rank, s);
									setColor((Player) t);
									s.sendMessage(t.getName() + "�7 ist nun " + ChatColor.translateAlternateColorCodes('&', color) + "�l" + rank + "�7.");
								}
								catch (NullPointerException e)
								{
									s.sendMessage("�cDieser Spieler ist bereits " + ChatColor.translateAlternateColorCodes('&', color) + "�l" + rank + "�c.");
								}
							}
							else
							{
								s.sendMessage("�cDieser Rang ist nicht registriert.");
							}
						}
						else
						{
								s.sendMessage("�7/chat setrank <Player> <Rank>");
						}
					}
					else if (args[0].equalsIgnoreCase("remrank"))
					{
						OfflinePlayer t = getServer().getOfflinePlayer(args[1]);
						
						this.sql.remRankFromPlayer((Player) t);
						s.sendMessage("�f�l" + t.getName() + " �7ist nun wieder �6�lSpieler.");
					}
				}
			}
			else
			{
				s.sendMessage("�7Du darfst dieses Kommando nicht nutzen.");
			}
			return true;
		}
		
		return false;
	}
	
	public void setColor(Player player)
	{
		String rank = this.sql.getRankFromPlayer(player.getName());
		String playername = player.getName();
		if (playername.length() > 14)
		{
			playername = playername.substring(0, 12);
		}
		if (rank.length() > 0)
		{
			String color = ChatColor.translateAlternateColorCodes('&', this.sql.getColor(rank));
			player.setDisplayName(color + playername);
			player.setPlayerListName(color + playername);
			//Team team = this.score.getTeam(rank);
			//team.addPlayer(player);
		}
		else
		{
			String color = ChatColor.translateAlternateColorCodes('&', "&6");
			player.setDisplayName(color + playername);
			player.setPlayerListName(color + playername);
			//Team team = this.score.getTeam("Spieler");
			//team.addPlayer(player);
		}
	}
		
		
	private boolean IsRankNameValid(String RankName)
	{
		boolean statement = false;
		
		if (RankName.length() > 20)
			statement = false;
		else
		{
			statement = true;
		}
		return statement;
	}
	
	
	public String escapeSpecialChar(String string)
	{
		for (int i = 0; i < this.specialChar.length; i += 2)
		{
			string = string.replaceAll(this.specialChar[i], this.specialChar[(i + 1)]);
		}
		
		return string;
	}
}