package me.mrreasole.chatmanager.bungee;

import me.mrreasole.chatmanager.ChatManager;

import org.bukkit.entity.Player;


import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;

public class Messager
{
	private ChatManager plugin;
	private MessageListener msgListener;
	
	public Messager(ChatManager pl)
	{
		this.plugin = pl;
		
		this.msgListener = new MessageListener(this.plugin);
		
		this.plugin.getServer().getMessenger().registerOutgoingPluginChannel(plugin, ChatManager.bungeeChannel);
		this.plugin.getServer().getMessenger().registerIncomingPluginChannel(plugin, ChatManager.bungeeChannel, msgListener);
	}
	
	public void forwardChatMessage(String msg, String pName)
	{
		ByteArrayDataOutput out = ByteStreams.newDataOutput();
		
		out.writeUTF("Forward");
		out.writeUTF("ALL");
		
		out.writeUTF(ChatManager.chatChannel);
		
		Player p = null;
		
		try
		{
			p = plugin.getServer().getOnlinePlayers()[0];
		}
		catch(ArrayIndexOutOfBoundsException ex)
		{
			return;
		}
		
		out.writeUTF(plugin.servername + ChatManager.splitChar + msg + ChatManager.splitChar + pName);
		
		p.sendPluginMessage(plugin, ChatManager.bungeeChannel, out.toByteArray());
	}
}