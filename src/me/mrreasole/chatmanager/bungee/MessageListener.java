package me.mrreasole.chatmanager.bungee;

import me.mrreasole.chatmanager.ChatManager;

import org.bukkit.entity.Player;
import org.bukkit.plugin.messaging.PluginMessageListener;


import com.google.common.io.ByteArrayDataInput;
import com.google.common.io.ByteStreams;

public class MessageListener implements PluginMessageListener
{
	private ChatManager plugin;
	
	public MessageListener(ChatManager pl)
	{
		this.plugin = pl;
	}
	
	@Override
	public void onPluginMessageReceived(String channel, Player p, byte[] message)
	{
		if (!channel.equals("BungeeCord"))
		{
			return;
		}
		
		ByteArrayDataInput in = ByteStreams.newDataInput(message);
		String subchannel = in.readUTF();
		
		if (subchannel.equals(ChatManager.chatChannel))
		{
			String[] unedited = in.readUTF().split(ChatManager.splitChar); 
			
			if(unedited.length < 2)
			{
				return;
			}
			
			String senderSvr = unedited[0];
			String msg = unedited[1];
			String pName = unedited[2];
			
			//System.out.println("Sender: " + senderSvr);
			
			if(senderSvr.equals(plugin.servername))
			{
				return;
			}
			else
			{
				String formatted = plugin.evt.formatMessage(msg, pName, senderSvr);
				
				//System.out.println(formatted);
				
				for(Player tmp : plugin.getServer().getOnlinePlayers())
				{
					tmp.sendMessage(formatted);
				}
			}
		}
	}
}
