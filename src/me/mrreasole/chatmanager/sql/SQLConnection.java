package me.mrreasole.chatmanager.sql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import me.mrreasole.chatmanager.ChatManager;

import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;


public class SQLConnection
{
  ChatManager chatManager = null;
  Connection con = null;
  Statement stmt = null;
  ResultSet rs = null;
  int res = 0;

  
  public void close()
  {
	  try
	  {
		  stmt.close();
		  con.close();
	  }
	  catch(Exception e)
	  {
		  
	  }
  }
  
  public SQLConnection(String host, String port, String user, String pass, String db) throws Exception
  {
    String url = "jdbc:mysql://" + host + ":" + port + "/" + db;
    try
    {
      Class.forName("com.mysql.jdbc.Driver").newInstance();
      this.con = DriverManager.getConnection(url, user, pass);
      this.stmt = this.con.createStatement();

      this.res = this.stmt.executeUpdate("create table if not exists Ranks(RankName VARCHAR(40) NOT NULL, Color VARCHAR(2) NOT NULL, Prefix VARCHAR(40) NOT NULL, Suffix VARCHAR(40), PRIMARY KEY (RankName))");

      this.res = this.stmt.executeUpdate("create table if not exists PlayerRanks(Name VARCHAR(40) NOT NULL, RankName VARCHAR(40) NOT NULL, PRIMARY KEY (Name))");

      this.stmt.close();
      System.out.println("Connected to SQL db!");
    } catch (java.lang.IllegalAccessException e) {
      e.printStackTrace();
    }
  }

  public String getPrefix(String rank)
  {
    String prefix = null;
    try {
      this.stmt = this.con.createStatement();
      this.rs = this.stmt.executeQuery("SELECT Prefix FROM Ranks WHERE RankName = '" + rank + "'");
      while (this.rs.next()) {
        prefix = this.rs.getString("Prefix");
      }
      this.stmt.close();
    } catch (SQLException e)
    {
      e.printStackTrace();
    } 
    return prefix;
  }

  public String getSuffix(String rank) {
    String suffix = null;
    try {
      this.stmt = this.con.createStatement();
      this.rs = this.stmt.executeQuery("SELECT Suffix FROM Ranks WHERE RankName = '" + rank + "'");
      while (this.rs.next()) {
        suffix = this.rs.getString("Suffix");
      }
      this.stmt.close();
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return suffix;
  }

  public String getColor(String rank) {
    String rankName = null;
    try {
      this.stmt = this.con.createStatement();
      this.rs = this.stmt.executeQuery("SELECT Color FROM Ranks WHERE RankName = '" + rank + "'");
      while (this.rs.next()) {
        rankName = this.rs.getString("Color");
      }
      this.stmt.close();
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return rankName;
  }

  public boolean rankExists(String rankToCheck)
  {
    boolean exists = false;
    try {
      this.stmt = this.con.createStatement();
      this.rs = this.stmt.executeQuery("SELECT RankName FROM Ranks WHERE RankName = '" + rankToCheck + "'");
      if (this.rs.next()) {
        exists = true;
      }

      this.stmt.close();
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return exists;
  }

  public String getRankFromPlayer(String playerName)
  {
    String rank = null;
    try
    {
      this.stmt = this.con.createStatement();
      this.rs = this.stmt.executeQuery("SELECT RankName FROM PlayerRanks WHERE Name = '" + playerName + "'");
      if (this.rs.next())
      {
        rank = this.rs.getString("RankName");
      }
      else
      {
    	  rank = "";
      }
      this.stmt.close();
    }
    catch (SQLException e)
    {
      e.printStackTrace();
    }
    return rank;
  }

  public void setRankToPlayer(OfflinePlayer t, String rank, CommandSender sender) {
    try {
      this.stmt = this.con.createStatement();
      this.res = this.stmt.executeUpdate("INSERT INTO PlayerRanks VALUES ('" + t.getName() + "', '" + rank + "') ON DUPLICATE KEY UPDATE RankName = '" + rank + "'");
      this.stmt.close();
    } catch (SQLException e) {
      e.printStackTrace();
    }
  }

  public void remRankFromPlayer(Player player) {
    try {
      this.stmt = this.con.createStatement();
      this.res = this.stmt.executeUpdate("DELETE FROM PlayerRanks WHERE Name = '" + player.getName() + "'");
      this.stmt.close();
    } catch (SQLException e) {
      e.printStackTrace();
    }
  }

  public void addRank(String rankName, String color, String prefix, String suffix) {
    try {
      this.stmt = this.con.createStatement();
      this.res = this.stmt.executeUpdate("INSERT INTO Ranks VALUES('" + rankName + "', '" + color + "', '" + prefix + "', '" + suffix + "')");
      //Team team = this.chatManager.getScoreboard().registerNewTeam(rankName);
      //team.setPrefix(ChatColor.translateAlternateColorCodes('&', color));
      this.stmt.close();
    } catch (SQLException e) {
      e.printStackTrace();
    }
  }

  public void delRank(String Name) {
    try {
      this.stmt = this.con.createStatement();
      this.res = this.stmt.executeUpdate("DELETE FROM Ranks WHERE RankName = '" + Name + "'");
      //Team team = this.chatManager.getScoreboard().getTeam(Name);
      //team.unregister();
      this.stmt.close();
    } catch (SQLException e) {
      e.printStackTrace();
    }
  }

  public void setChatManager(ChatManager chatManager)
  {
    this.chatManager = chatManager;
  }
}